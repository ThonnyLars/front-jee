import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Header extends React.Component { 
    state = {
        user: Object
    }
    componentDidMount(){
        
            this.setState({
                user:this.props.user
             });
      
    }
render() {
    console.log(this.props)
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand" href="#"> ISI</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                    <Link><a className="nav-link">Accueil<span className="sr-only">(current)</span></a></Link>
                    </li>
                    <li className="nav-item">
                    <Link to="/Bien"><a className="nav-link">Biens</a></Link>
                    </li>
                    <li className="nav-item">
                        <Link><a className="nav-link">Locations</a></Link>
                    </li>
                    <li className="nav-item">
                    <Link><a className="nav-link">A propos</a></Link>
                    </li>
                </ul>
                <span className="navbar-text">
                 {this.state.user.nom +' '+ this.state.user.prenom}  
                </span>
            </div>
        </nav>)
}
}


const mapStateToProps = (state) =>{
    return{
        user:state
    }
}

export default connect(mapStateToProps)(Header)