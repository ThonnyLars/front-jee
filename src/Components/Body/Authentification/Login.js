import React from 'react';
import axios from 'axios';
import '../../../CSS/login.css';
import { GetUser } from '../../../Services/Call';
import { connect } from 'react-redux';

class Login extends React.Component {

    constructor() {
        super();

        this.state = {
            bool: false,
            userEmail: '',
            userpwd: '',
            conn: ''
        }
    }
    getEmail(event) {
        this.setState({
            userEmail: event.target.value
        });
    }
    getPwd(event) {
        this.setState({
            userpwd: event.target.value
        });
    }

    Connexion(event) {
        event.preventDefault();
        this.setState({
            conn: ''
        });
        const user = {
            id: 0,
            matricule: '',
            nom: '',
            prenom: '',
            login: this.state.userEmail,
            mdp: this.state.userpwd
        }
        console.log(user);

        
        this.setState({
            conn: 'Veuillez Patienter ...'
        })
        axios.post("http://localhost:21783/projet_jee/api/user/addUser", user).then(res => {
            console.log(res.data);
            if (res.data === null || res.data === 1) {
                this.setState({
                    conn: 'Veuillez vérifier vos informations de connexion'
                })
            } else {
                this.setState({
                    conn: res.data.nom + ' - ' + res.data.prenom
                });
                this.props.GetUser(res.data);
                this.props.history.push("/Home/");
            }
        });
    }

    render() {
        return (
            <div>
                <div className="container mt-5">
                    <h4 className="erreur">{this.state.conn}</h4>
                    <div className="row">
                        <div className="card mx-auto mt-5">
                            <div className="card-body">
                                <form className="text-center p-5">
                                    <div className="form-group">
                                        <label>Entrer votre Email</label>
                                        <input
                                            value={this.state.userEmail}
                                            autoComplete="off"
                                            type="email"
                                            className="form-control"
                                            onChange={this.getEmail.bind(this)}
                                            required
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Entrer votre mot de passe</label>
                                        <input
                                            value={this.state.userpwd}
                                            autoComplete="off"
                                            type="password"
                                            className="form-control"
                                            onChange={this.getPwd.bind(this)}
                                            required
                                        />
                                    </div>
                                    <div className="form-group justify-content-center">
                                        <button type="submit" onClick={this.Connexion.bind(this)} className="btn btn-primary">Valider</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        GetUser: (user) => dispatch(GetUser(user))
    }
}

export default connect(null, mapDispatchToProps)(Login)