import React from 'react';
import Header from '../../Header/Header';
import axios from 'axios';
import '../../../CSS/stepOne.css';
import { Footer } from '../../Footer/Footer';

export class StepOne extends React.Component {

    constructor() {
        super();
        this.state = {
            id: 0,
            cni: '',
            check: '',
            nom: '',
            prenom: '',
            rs: '',
            checkTel: '',
            email: '',
            adresse: '',
            tel: '',
            type: '',
            bool: false,
            bien: Object,
            typePersone: [Object],
            info: 'Veuillez Patienter ...',
            validate:''
        }
    }

    //Recherche du bien pour voir si il est en location ou pas
    async componentDidMount() {
        //Récupération du bien s'il n'est pas en location
        const url1 = "http://localhost:21783/projet_jee/api/location/find/" + this.props.match.params.id;
        const response = await fetch(url1);
        const data = await response.json();

        //Liste des types de personnes
        const url2 = "http://localhost:21783/projet_jee/api/location/typePersonne";
        const response1 = await fetch(url2);
        const data2 = await response1.json();

        if (data.id === 0) {
            this.setState({
                info: 'Ce bien est dans une location en cours'
            })
        } else {
            this.setState({
                info: '',
                bool: true,
                bien: data,
                typePersone: data2
            })
        }
    }

    //Récupération du type de personne
    getGenreType(event) {
        this.setState({
            type: event.target.value
        })
    }

    //Récupération des valeurs du formulaire
    getValue(event) {
        this.setState({
            [event.target.id]: event.target.value
        })


    }

    //On vérifie si le client existe déjà
    async IfExist(event) {
        event.preventDefault();
        console.log(this.state.cni);
        const url1 = "http://localhost:21783/projet_jee/api/location/testClient/" + this.state.cni;
        const response = await fetch(url1);
        const data = await response.json();
        this.setState({
            check: 'Veuillez Patienter ...'
        })
        if (data.id === 0) {
            this.setState({
                id:0,
                nom: '',
                prenom: '',
                rs: '',
                email: '',
                adresse: '',
                tel: '',
                check: 'Aucune correspondance trouvée dans la base de données'
            })
        } else {
            console.log(data);
            this.setState({
                cni: data.cni,
                id: data.id,
                check: 'Utilisateur Trouvé',
                nom: data.nom,
                prenom: data.prenom,
                rs: data.rs,
                email: data.email,
                adresse: data.adresse,
                tel: data.tel,
                type: data.type,
            })
        }
    }

    //Valider la location
    Validate(event) {
        event.preventDefault();
        this.setState({
            validate: 'Veuillez Patienter ...'
        })
        const location = {
            bien: this.props.match.params.id,
            cni: this.state.cni,
            id: this.state.id,
            nom: this.state.nom,
            prenom: this.state.prenom,
            rs: this.state.rs,
            email: this.state.email,
            adresse: this.state.adresse,
            tel: this.state.tel,
            type: this.state.type
        }

        //console.log(location);

        axios.post("http://localhost:21783/projet_jee/api/location/addLocation", location).then(res => {
            console.log(res.data);
            this.props.history.push("/Facture/"+res.data);
        });
    }


    render() {
        if (this.state.bool === false || this.state.bien.id === 0) {
            return (
                <div>
                    <Header />
                    <h4>{this.state.info} </h4>
                </div>
            )
        } else {
            return (
                <div>
                    <Header />
                    <div className="row col-12 espace">
                        <div className="row col-12">
                            <div className="col-lg-7">
                                <h4 className="font-weight-bold">Résumé</h4>
                            </div>
                            <div className="col-lg-5">
                                <p>
                                    Le Lorem Ipsum est simplement du faux texte employé dans la composition
                                    et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard
                                    de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla
                                    ensemble des morceaux de texte pour réaliser un livre spécimen de polices de
                                    texte.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="row col-12 space">
                        <div className="row col-12">
                            <p className="resume">Veuillez renseigner les informations du locataire</p>
                        </div>
                    </div>
                    <form className="row col-12 mt-4">
                        {/* <form> */}
                        <div className="row col-12 mt-3">
                            <div className="col-lg-4">
                                <label>CNI / Matricule Entreprise </label>
                                <input
                                    value={this.state.cni}
                                    autoComplete="off"
                                    type="text"
                                    id="cni"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}

                                />
                            </div>
                            <div className="col-lg-4 btnSpace">
                                <button className="btn btn-primary float-right" onClick={this.IfExist.bind(this)}>Vérifier</button>
                            </div>
                            <div className="col-lg-4">
                                <p className="check">{this.state.check}</p>
                            </div>

                        </div>

                        <div className="row col-12 mt-3">
                            {/* Type de personne */}
                            <div className="col-lg-4 col-xs-12">
                                <label>Genre</label>
                                <select onChange={this.getGenreType.bind(this)}
                                    name="TypePersonne" id="TypePersonne"
                                    className="form-control custom-select">
                                    <option value="">Choisir</option>
                                    {this.state.typePersone.map(e => (
                                        <option key={e.id} value={e.id}>{e.lib}</option>
                                    ))}
                                </select>
                            </div>

                            {/* Nom */}
                            <div className="col-lg-4 col-xs-12">
                                <label>Nom</label>
                                <input
                                    value={this.state.nom}
                                    autoComplete="off"
                                    type="text"
                                    id="nom"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}

                                />
                            </div>

                            {/* Prénom */}
                            <div className="col-lg-4 col-xs-12">
                                <label>Prénom</label>
                                <input
                                    value={this.state.prenom}
                                    autoComplete="off"
                                    type="text"
                                    id="prenom"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}

                                />
                            </div>
                        </div>

                        <div className="row col-12 mt-3">
                            {/* Email */}
                            <div className="col-lg-4 col-xs-12">
                                <label>Email</label>
                                <input
                                    value={this.state.email}
                                    autoComplete="off"
                                    type="Email"
                                    id="email"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}

                                />
                            </div>

                            {/* Adresse*/}
                            <div className="col-lg-4 col-xs-12">
                                <label>Adresse</label>
                                <input
                                    value={this.state.adresse}
                                    autoComplete="off"
                                    type="text"
                                    id="adresse"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}

                                />
                            </div>

                            {/* Téléphone */}
                            <div className="col-lg-4 col-xs-12">
                                <label>Téléphone</label>
                                <input
                                    value={this.state.tel}
                                    autoComplete="off"
                                    type="text"
                                    id="tel"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}
                                />
                                <p className="checkTel">{this.state.checkTel}</p>
                            </div>
                        </div>

                        <div className="row col-12 mt-3">
                            {/* Raison social*/}
                            <div className="col-lg-4 col-xs-12">
                                <label>Raison social</label>
                                <textarea
                                    value={this.state.rs}
                                    autoComplete="off"
                                    id="rs"
                                    className="form-control"
                                    onChange={this.getValue.bind(this)}

                                />
                            </div>

                            
                            <div className="col-lg-4 col-xs-12 btnSpace">
                                <button className="btn btn-success" onClick={this.Validate.bind(this)}>Valider</button>
                            </div>

                             <div className="col-lg-4">
                                <p className="check">{this.state.validate}</p>
                            </div>
                        </div>


                        {/* </form> */}
                    </form>
                    <Footer />
                </div>
            )
        }
    }
}