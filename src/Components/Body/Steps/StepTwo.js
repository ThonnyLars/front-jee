import React from 'react';
import Header from '../../Header/Header';
import axios from 'axios';
import '../../../CSS/stepTwo.css';
import { Footer } from '../../Footer/Footer';

export class StepTwo extends React.Component {
    constructor() {
        super();
        this.state = {
            factureEntree: Object,
            Caution: Object,
            moyenPaiement: [Object],
            moyenDePaiement:''
        }
    }

    async componentDidMount() {
        //Récupération de la facture d'entré
        const url1 = "http://localhost:21783/projet_jee/api/Facture/Entree/" + this.props.match.params.location;
        const response = await fetch(url1);
        const data = await response.json();

        //Récupération de la caution
        const url2 = "http://localhost:21783/projet_jee/api/Facture/Caution/" + this.props.match.params.location;
        const response1 = await fetch(url2);
        const data2 = await response1.json();

        //Récupération Moyen de paiement
        const url3 = "http://localhost:21783/projet_jee/api/Facture/MoyenPaiement";
        const response2 = await fetch(url3);
        const data3 = await response2.json();

        this.setState({
            factureEntree: data,
            Caution: data2,
            moyenPaiement: data3
        })
    }

    getmoyenPaiement(e){
        this.setState({
            moyenDePaiement: event.target.value
        })
    }

    Validate(e){
        e.preventDefault();
        const paiement ={
            idFacture:this.state.factureEntree.id,
            idCaution:this.state.Caution.id,
            typePaiement:this.state.moyenDePaiement,
            montant:this.state.factureEntree.montant+this.state.Caution.montant
        }

        axios.post("http://localhost:21783/projet_jee/api/Facture/facture", paiement).then(res => {
            console.log(res.data);
            ///this.props.history.push("/Facture/"+res.data);
        });
    }
    render() {
        return (
            <div>
                <Header />
                <div className="row col-12 espace">
                    <div className="row col-12 mt-4">
                        <h4 className="justify-content-center">Factures d'entrée</h4>
                    </div>
                    <div className="row col-12 mt-4">
                        <form className="row col-12 mt-4">
                            <div className="col-lg-4">
                                <label>Moyen de paiement</label>
                                <select onChange={this.getmoyenPaiement.bind(this)}
                                    name="moyenDePaiement" id="moyenDePaiement"
                                    className="form-control custom-select">
                                    <option value="">Choisir</option>
                                    {this.state.moyenPaiement.map(e => (
                                        <option key={e.id} value={e.id}>{e.lib}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="col-lg-8">
                            <button className="btn btn-success float-right" onClick={this.Validate.bind(this)}>Payer</button>
                            </div>
                        </form>
                    </div>
                    <div className="row col-12 mt-4">
                        <div className="col-lg-6">
                            <p>Facture d'entrée</p>
                        </div>

                        <div className="col-lg-6">
                            <p>Facture de caution</p>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>)
    }
}