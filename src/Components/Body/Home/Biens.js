import React from 'react';
import Header from '../../Header/Header';
import '../../../CSS/Bien.css';
import { Footer } from '../../Footer/Footer';
import axios from 'axios';

export class Bien extends React.Component {
    constructor() {
        super();
        this.state = {
            id: 0,
            cni: '',
            check: '',
            nom: '',
            prenom: '',
            rs: '',
            checkTel: '',
            email: '',
            adresse: '',
            tel: '',
            type: '',
            typePersone: [Object],
            surface: '',
            nbreChambre: '',
            adresseB: '',
            bien: '',
            Villes: [Object],
            ville: '',
            MontantLocation: '',
            info: '',
            typeBat: [Object]
        }
    }

    async componentDidMount() {
        //Liste des types de personnes
        const url2 = "http://localhost:21783/projet_jee/api/location/typePersonne";
        const response1 = await fetch(url2);
        const data2 = await response1.json();
        this.setState({
            typePersone: data2
        })

        //Liste des villes
        const url = "http://localhost:21783/projet_jee/api/bien/villes";
        const response = await fetch(url);
        const data = await response.json();
        this.setState({
            Villes: data
        })

        //Liste du type de bien
        const url3 = "http://localhost:21783/projet_jee/api/bien/typeBien";
        const response2 = await fetch(url3);
        const data3 = await response2.json();
        this.setState({
            typeBat: data3
        })

        
    }

    //Récupération du type de personne
    getGenreType(event) {
        this.setState({
            type: event.target.value
        })
    }

    //Récupération du type de bien
    getValueBienType(event) {
        this.setState({
           bien: event.target.value
        })
    }

    //Récupération de la ville
    getValueVille(event) {
        this.setState({
            ville: event.target.value
        })
    }

    //Réecupération des infos du formulaire
    getValue(event) {
        this.setState({
            [event.target.id]: event.target.value
        })
    }



    //Vérification de l'existence du proprio
    async IfExist(event) {
        event.preventDefault();
        console.log(this.state.cni);
        const url1 = "http://localhost:21783/projet_jee/api/location/testClient/" + this.state.cni;
        const response = await fetch(url1);
        const data = await response.json();

        if (data.id === 0) {
            this.setState({
                id:0,
                check: 'Aucune correspondance trouvée dans la base de données'
            })
        } else {
            this.setState({
                cni: data.cni,
                id: data.id,
                check: 'Utilisateur Trouvé',
                nom: data.nom,
                prenom: data.prenom,
                rs: data.rs,
                email: data.email,
                adresse: data.adresse,
                tel: data.tel,
                type: data.type,
            })
        }
    }

    Validate(event){
        event.preventDefault();
      

        const AjoutBien = {     
            /* Infos relatives au client */
            cni: this.state.cni,
            idPers: this.state.id,
            nom: this.state.nom,
            prenom: this.state.prenom,
            rs: this.state.rs,
            email: this.state.email,
            adressePers: this.state.adresse,
            tel: this.state.tel,
            typePers: this.state.type,
            
            /* Infos relatives au bien */
            id:0,
            nbreChambre: this.state.nbreChambre,
            ville:this.state.ville,
            type:this.state.bien,
            info:this.state.info,
            surface:this.state.surface,
            adresse:this.state.adresseB,
            montant:this.state.MontantLocation
        }

        console.log(AjoutBien);
        axios.post("http://localhost:21783/projet_jee/api/bien/addBien", AjoutBien).then(res => {
            console.log(res.data);
        });
    }

    render() {
        return (
            <div>
                <Header />
                <div className="row col-12 espace">
                    <div className="row col-12 mt-4">
                        <h4>Veuillez renseigner les infos du propriétaires</h4>
                        <form className="row col-12 mt-4">
                            <div className="row col-12 mt-3">
                                <div className="col-lg-4">
                                    <label>CNI / Matricule Entreprise </label>
                                    <input
                                        value={this.state.cni}
                                        autoComplete="off"
                                        type="text"
                                        id="cni"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}
                                    />
                                </div>
                                <div className="col-lg-4 btnSpace">
                                    <button className="btn btn-primary float-right" onClick={this.IfExist.bind(this)}>Vérifier</button>
                                </div>
                                <div className="col-lg-4">
                                    <p className="check">{this.state.check}</p>
                                </div>

                            </div>


                        </form>

                        <form className="row col-12 mt-4">
                            <div className="row col-12 mt-3">
                                {/* Type de personne */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Genre</label>
                                    <select onChange={this.getGenreType.bind(this)}
                                         id="TypePersonne"
                                        className="form-control custom-select">
                                        <option value="">Choisir</option>
                                        {this.state.typePersone.map(e => (
                                            <option key={e.id} value={e.id}>{e.lib}</option>
                                        ))}
                                    </select>
                                </div>

                                {/* Nom */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Nom</label>
                                    <input
                                        value={this.state.nom}
                                        autoComplete="off"
                                        type="text"
                                        id="nom"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                {/* Prénom */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Prénom</label>
                                    <input
                                        value={this.state.prenom}
                                        autoComplete="off"
                                        type="text"
                                        id="prenom"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>
                            </div>

                            <div className="row col-12 mt-3">
                                {/* Email */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Email</label>
                                    <input
                                        value={this.state.email}
                                        autoComplete="off"
                                        type="Email"
                                        id="email"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                {/* Adresse*/}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Adresse</label>
                                    <input
                                        value={this.state.adresse}
                                        autoComplete="off"
                                        type="text"
                                        id="adresse"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                {/* Téléphone */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Téléphone</label>
                                    <input
                                        value={this.state.tel}
                                        autoComplete="off"
                                        type="text"
                                        id="tel"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}
                                    />
                                    <p className="checkTel">{this.state.checkTel}</p>
                                </div>
                            </div>

                            <div className="row col-12 mt-3">
                                {/* Raison social*/}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Raison social</label>
                                    <textarea
                                        value={this.state.rs}
                                        autoComplete="off"
                                        id="rs"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                {/* <div className="col-lg-4 col-xs-12 btnSpace">
                                    <button className="btn btn-success" onClick={this.Validate.bind(this)}>Valider</button>
                                </div> */}
                            </div>


                        </form>
                    </div>

                    <div className="row col-12 mt-4">
                        <div>
                            <h4>Veuillez renseigner les informations relatives au bien</h4>
                        </div>

                        <form className="row col-12 mt-4">
                            <div className="row col-12 mt-3">
                                {/* Type de bien */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Type de bien</label>
                                    <select onChange={this.getValueBienType.bind(this)}
                                        id="bien"
                                        className="form-control custom-select">
                                        <option value="">Choisir</option>
                                        {this.state.typeBat.map(e => (
                                            <option key={e.id} value={e.id}>{e.lib}</option>
                                        ))}
                                    </select>
                                </div>

                                {/* Ville */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Localité</label>
                                    <select onChange={this.getValueVille.bind(this)}
                                        id="ville"
                                        className="form-control custom-select">
                                        <option value="">Choisir</option>
                                        {this.state.Villes.map(e => (
                                            <option key={e.id} value={e.id}>{e.lib}</option>
                                        ))}
                                    </select>
                                </div>

                                {/* Prénom */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Adresse du bien</label>
                                    <input
                                        value={this.state.adresseB}
                                        autoComplete="off"
                                        type="text"
                                        id="adresseB"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}
                                    />
                                </div>
                            </div>

                            <div className="row col-12 mt-3">
                                {/* Nombre de chambre */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Nombre de chambre</label>
                                    <input
                                        value={this.state.nbreChambre}
                                        autoComplete="off"
                                        type="text"
                                        id="nbreChambre"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                {/* Surface */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Surface</label>
                                    <input
                                        value={this.state.surface}
                                        autoComplete="off"
                                        type="text"
                                        id="surface"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                {/* Téléphone */}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Montant de location</label>
                                    <input
                                        value={this.state.MontantLocation}
                                        autoComplete="off"
                                        type="text"
                                        id="MontantLocation"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}
                                    />
                                    <p className="checkTel">{this.state.checkTel}</p>
                                </div>
                            </div>

                            <div className="row col-12 mt-3">
                                                               {/* Raison social*/}
                                <div className="col-lg-4 col-xs-12">
                                    <label>Infos supplémentaires</label>
                                    <textarea
                                        value={this.state.info}
                                        autoComplete="off"
                                        id="info"
                                        className="form-control"
                                        onChange={this.getValue.bind(this)}

                                    />
                                </div>

                                <div className="col-lg-4 col-xs-12 btnSpace">
                                    <button className="btn btn-success" onClick={this.Validate.bind(this)}>Valider</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}