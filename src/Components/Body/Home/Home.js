import React from 'react';
import Header from '../../Header/Header';
import '../../../CSS/Home.css';
import axios from 'axios';

export class Home extends React.Component {
    constructor() {
        super();

        this.state = {
            bool: false,
            bien: [],
            info: 'Veuillez Patienter ...'
        }
        //this.getParamater.bind(this);
    }
    async componentDidMount() {
        const url1 = "http://localhost:21783/projet_jee/api/bien/All";
        const response = await fetch(url1);
        const data = await response.json();
        //console.log(data);
        this.setState({
            bien: data
        })
    }

    getParamater(id){
        //e.preventDefault();
        console.log(id);
        this.props.history.push("/stepOne/"+id);
    }

    render() {
        //console.log(this.state.bien);
        return (
            <div>
                <Header />
                <div className='row col-12 espace'>
                <div className='row col-12'> 
                    <div className='col-lg-7'>
                        <h4>Retrouver ci dessous tous les appartements et chambres dispo</h4>
                    </div>
                    <div className='col-lg-5'>
                       <button className='btn btn-primary float-right'><a className='fa fa-phone'></a>Ajouter</button>
                    </div>
                </div>
               
                </div>
                <div className="row col-12 espace">
                {this.state.bien.map(e => (
                            <div className="col-md-3" key={e.id}>
                                <div className="card-wrapper">
                                    <div id="card-1" className="card-rotating effect__click text-center h-100 w-100 z-depth-1">

                                        <div className="face front card taille">
                                            <div className="card-up card-up-bb">
                                                <div className="image_size">

                                                </div>
                                                {/* Pour les images avec un slide show */}
                                                {/* <img className="card-img-top image_size" src={this.state.base64 + e.img} alt="image" /> */}
                                            </div>
                                            <div className="font-weight-bold white-custom vol-location text-left">


                                            </div>
                                            <div className="avatar mx-auto white">
                                                <i className="fa fa-home rounded-circle icon-result"></i>
                                            </div>

                                            <div className="card-body card-body-content">
                                                <h6 className="font-weight-bold"></h6>
                                                <div className="container-fluid">
                                                    <div className="row">
                                                        <span className="font-weight-bold col-md-12 font-size-15  blue-text">{e.type}</span>
                                                        <p className="col-md-12  font-size-14 blue-text">Sur une surface de {e.surface} m²</p>
                                                        <p className="col-md-12  font-size-14 blue-text">A partir de {e.MontantLocation} FCFA</p>
                                                        <p className="col-md-12 font-size-14 blue-text"> Situé à {e.adresse} - {e.ville}</p>
                                                        <p className="col-md-12 font-size-14 blue-text"> Info Supplémentaire {e.info}</p>
                                                        <p className="col-md-12 font-size-14 blue-text">{e.info}</p>
                                                    </div>
                                                </div>
                                                <div className="container-fluid">
                                                <button onClick={this.getParamater.bind(this,e.id)} className="btn btn-success float-right">Consulter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                </div>
            </div>

        )
    }
}