import React from 'react';

export class Footer extends React.Component {
    render() {
        return (
            <footer className="page-footer font-small wow fadeIn">
                <div className="footer-copyright mt-4">
                    <a href="">ISI © Copyright 2019</a>
                </div>
                {/* <Header/> */}
            </footer>
        );
    }
}