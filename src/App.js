import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Login from './Components/Body/Authentification/Login';
import { Home } from './Components/Body/Home/Home';
import { Footer } from './Components/Footer/Footer';
import { StepOne } from './Components/Body/Steps/StepOne';
import { Bien } from './Components/Body/Home/Biens';
import { StepTwo } from './Components/Body/Steps/StepTwo';

function App() {
  return (
    <div className="App">
    <Router>
       <Route path={"/"} component={Login} exact />
       <Route path={"/Home"} component={Home} />
      {/*  <Route path={"/f"} component={Footer} /> */}
       <Route path={"/stepOne/:id"} component={StepOne} />
       <Route path={"/Bien"} component={Bien} />
       <Route path={"/Facture/:location"} component={StepTwo} />
     </Router>
   </div>
  );
}

export default App;
